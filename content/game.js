$( document ).ready( function() {
    //console.log( data );
    var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
    var monthsLong = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var day = 1;
    var month = 1;
    var year = 2018;

    var deadCount = 0;
    var shootingCount = 0;

    var key = "";
    var todayData = null;

    var happySound = new Audio( "content/188714__waveplay-old__happy-effect-3.wav" );
    happySound.loop = false;
    var sadSound = new Audio( "content/51752__erkanozan__warning.wav" );
    sadSound.loop = false;

    $( ".phone-view" ).click( function() {
        if ( $( ".locked-view" ).css( "display" ) == "none" ) {
            Lock();
        }
        else
        {
            Unlock();
        }
        } );

    function UpdateDay() {
        key = monthsLong[ month - 1 ] + " " + day + ", 2018";
        if ( key in data )
        {
            todayData = data[ key ];
        }
        else
        {
            todayData = null;
        }
        
        $( ".day" ).html( day );
        $( ".month" ).html( months[ month-1 ] );
        $( ".headline" ).remove();

        if ( todayData == null )
        {
            // No shooting
        }
        else
        {
            // Shooting
            for ( var i = 0; i < todayData.length; i++ )
            {
                var string = "<strong>Shooting in " + todayData[i]["area"] + ", " + todayData[i]["state"] + "</strong><br>" + todayData[i]["killed"] + " killed, " + todayData[i]["injured"] + " injured.";
                $( ".news .text" ).append( "<p class='headline'>" + string + "</p>" );

                shootingCount++;
                deadCount += parseInt( todayData[i]["killed"] );
            }
        }

    }

    function NewDay() {
        day++;
        
        if ( day == 32 && ( month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12 ) )
        {
            day = 1;
            month++;
            if ( month == 6 )
            {
                $( ".phone-view" ).removeClass( "spring" );
                $( ".phone-view" ).addClass( "summer" );
            }
            else if ( month == 8 )
            {
                $( ".phone-view" ).removeClass( "summer" );
                $( ".phone-view" ).addClass( "autumn" );
            }
            else if ( month == 11 )
            {
                $( ".phone-view" ).removeClass( "autumn" );
                $( ".phone-view" ).addClass( "holiday" );
            }
        }
        else if ( day == 31 && ( month == 4 || month == 6 || month == 9 || month == 11 ) )
        {
            day = 1;
            month++;
        }
        else if ( day == 29 && month == 2 )
        {
            day = 1;
            month++;
            if ( month == 3 )
            {
                $( ".phone-view" ).removeClass( "winter" );
                $( ".phone-view" ).addClass( "spring" );
            }
        }

        UpdateDay();
    }

    function Unlock() {
        $( ".phone-view" ).removeClass( "clear" );
        $( ".phone-view" ).addClass( "blur" );

        if ( todayData == null )
        {
            $( ".good-morning" ).css( "display", "block" );
            $( ".news" ).css( "display", "none" );

            // play sound
            happySound.play();
        }
        else
        {
            $( ".good-morning" ).css( "display", "none" );
            $( ".news" ).css( "display", "block" );

            // play sound
            sadSound.play();
        }

        $( ".body-count" ).html( deadCount );
        $( ".shooting-count" ).html( shootingCount );
        
        
        $( ".locked-view" ).slideUp( "fast" );
        $( ".unlocked-view" ).slideDown( "fast" );
    }

    function Lock() {
        NewDay();
        $( ".phone-view" ).addClass( "clear" );
        $( ".phone-view" ).removeClass( "blur" );
        
        $( ".unlocked-view" ).slideUp( "fast" );
        $( ".locked-view" ).slideDown( "fast" );
    }

    UpdateDay();
} );
