# Good Morning 2018

## Credits

Data is from [GunViolenceArchive.org](https://www.gunviolencearchive.org/reports/mass-shooting)

Background images from [Pexels](https://www.pexels.com/)

* Winter: [Christmas Cold Friends by Pixabay](https://www.pexels.com/photo/christmas-cold-friends-frosty-269370/)
* Spring: [Nature Flowers Blue Summer by Pixabay](https://www.pexels.com/photo/nature-flowers-blue-summer-40797/)
* Summer: [Animal Beach Coast Coastline by Pixabay](https://www.pexels.com/photo/animal-beach-coast-coastline-414105/)
* Autumn: [Autumn Colors by Pixabay](https://www.pexels.com/photo/autumn-autumn-colours-autumn-leaves-beautiful-355302/)
* Holidays: [Red Lighted Candle by Nubia Navarro](https://www.pexels.com/photo/red-lighted-candle-714898/)

Audio from [FreeSound.org](https://freesound.org/)

* Happy sound: [happy effect 3 by waveplay_old](https://freesound.org/people/waveplay_old/sounds/188714/)
* Sad sound: [warning.wav by erkanozan](https://freesound.org/people/erkanozan/sounds/51752/)
