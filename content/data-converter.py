import csv

headers = []
data = []
newData = {}

with open( "data.csv" ) as csvFile:
    csvReader = csv.reader( csvFile, delimiter=',' )
    line = 0

    for row in csvReader:
        for col in row:
            if ( line == 0 ):
                headers.append( col )
            else:
                data.append( col )

        if ( row[0] not in newData ):
            newData[ row[0] ] = []

        newData[ row[0] ].append( { "state" : row[1], "city" : row[2], "killed" : row[4], "injured" : row[5] } )
        

        line += 1

textFile = open( "data.js", "w" )
textFile.write( "var data = { \n" )

firstDate = True

def Space( string, length ):
    space = ""
    for i in range( length - len( string ) ):
        space += " "

    return space

for key, row in newData.iteritems():

    if ( firstDate == False ):
        textFile.write( ", \n" )
        
    textFile.write( "\t '" + key + "'" + Space( key, 20 ) + " : [ " )

    firstIncident = True
    for incident in row:
        print( incident )
        if ( firstIncident == False ):
            textFile.write( "," )
        else:
            textFile.write( "" )
            
        textFile.write( "\t { " );
        textFile.write( "\t\t 'state' : '" + incident["state"] + "', 'area' : '" + incident['city'] + "', 'killed' : '" + incident["killed"] + "', 'injured' : '" + incident["injured"] + "'" )
        textFile.write( "\t }" );
        firstIncident = False

    textFile.write( "]" )
    firstDate = False
    

textFile.write( "\n};" )
textFile.close()
